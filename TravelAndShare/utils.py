import hashlib
from random import choice

MIN_USERNAME_LEN = 4
MIN_PASSWORD_LEN = 4


def get_random_alphanumeric_string(length):
    alphabet = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
    return ''.join(choice(alphabet) for i in range(length))


def is_valid_username(username):
    allowed_chars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ._"
    if len(username) < MIN_USERNAME_LEN:
        return False
    for c in username:
        if c not in allowed_chars:
            return False
    return True


def is_valid_password(password):
    return len(password) >= MIN_PASSWORD_LEN


def get_hash(password):
    """
    :param password: User's password
    :return: Generated hash with salt (string)
    """
    salt = get_random_alphanumeric_string(16)
    hash_string = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt.encode('utf-8'), 100000).hex()
    return salt + hash_string


def verify_password(password, hashed_password):
    """
    :param password: User's password
    :param hashed_password: User's hashed password stored in database
    :return: Generated hash with salt (string)
    """
    salt = hashed_password[:16]
    new_hash = hashlib.pbkdf2_hmac('sha256', password.encode('utf-8'), salt.encode('utf-8'), 100000).hex()
    return new_hash == hashed_password[16:]


def create_token():
    return get_random_alphanumeric_string(32)
