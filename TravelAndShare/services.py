import base64
import os
import sys
from pathlib import Path

import pymongo
from bson.objectid import ObjectId
from time import time
from PIL import Image

import utils
from bson.binary import Binary

# mongo_client = pymongo.MongoClient("mongodb://localhost:27017/")
mongo_client = pymongo.MongoClient(os.environ["MONGODB_URI"])
db = mongo_client["via_travel_and_share"]  # database
USERS_DB = db["users"]  # collection
POSTS_DB = db["posts"]
IMAGES_DB = db["images"]


# USERS
def create_user(username, password):
    user = {
        "username": username,
        "username_l": username.lower(),
        "password": utils.get_hash(password),
        "token": utils.create_token(),
        "token_exp": int(time()) + 3600,  # User is valid for one hour
        "follow": []
    }
    USERS_DB.insert_one(user)
    return user


def delete_user(username):
    u_lower = username.lower()
    # 1) Delete posts
    posts_mongo = POSTS_DB.find({"username_l": u_lower}, {"_id": 1})
    for post in posts_mongo:
        delete_post(str(post.get("_id")))
    # 2) Delete follow relationships
    # this part could probably be implemented faster, but I'm not MongoDB expert...
    username = USERS_DB.find_one({"username_l": u_lower})["username"]
    followers = USERS_DB.find({"follow": {"$in": [username]}})
    for follower in followers:
        follow_list = follower["follow"]
        follow_list.remove(username)
        USERS_DB.update_one({"username_l": follower["username_l"]},
                            {"$set": {"follow": follow_list}})
    # 3) Delete user
    USERS_DB.delete_one({"username_l": u_lower})


def follow_user(follower_username, followed_username):
    u1 = follower_username.lower()
    u2 = followed_username.lower()
    if u1 == u2:
        return 1
    follower = USERS_DB.find_one({"username_l": u1})
    followed = USERS_DB.find_one({"username_l": u2})
    if followed is None:
        return 2
    follow_list = follower["follow"]
    username = followed["username"]
    if username in follow_list:
        return 3
    else:
        follow_list.append(username)
        USERS_DB.update_one({"username_l": u1}, {"$set": {"follow": follow_list}})
        return 0


def unfollow_user(follower_username, followed_username):
    u1 = follower_username.lower()
    u2 = followed_username.lower()
    if u1 == u2:
        return 1
    follower = USERS_DB.find_one({"username_l": u1})
    followed = USERS_DB.find_one({"username_l": u2})
    if followed is None:
        return 2
    follow_list = follower["follow"]
    username = followed["username"]
    if username not in follow_list:
        return 3
    else:
        follow_list.remove(username)
        USERS_DB.update_one({"username_l": u1}, {"$set": {"follow": follow_list}})
        return 0


def get_user(username):
    return USERS_DB.find_one({"username_l": username.lower()})


def verify_user(username, password):
    u_lower = username.lower()
    user = USERS_DB.find_one({"username_l": u_lower})
    if user is None:
        return None
    user_ok = utils.verify_password(password, user["password"])
    if not user_ok:
        return None
    user["token"] = utils.create_token()
    user["token_exp"] = int(time()) + 3600
    USERS_DB.update_one({"username_l": u_lower}, {"$set": {"token": user["token"], "token_exp": user["token_exp"]}})
    return user


def user_in_database(username):
    return get_user(username) is not None


def authorize(headers):
    if 'Authorization' in headers:
        encoded_uname_pass = headers['Authorization'].split()[-1]  # last substring
        username_password = base64.b64decode(encoded_uname_pass).decode('utf-8').split(":")
        if verify_token(username_password[0], username_password[1]):
            return get_user(username_password[0])
    return None


# POSTS
def create_post(username, title, text, map_coords, image):
    user = get_user(username)
    post = {
        "date_created": int(time()),
        "username": user["username"],
        "username_l": user["username_l"],
        "title": title,
        "text": text,
        "map_coords": map_coords,
        "image": image
    }
    if image is not None:
        upload_image(image)
        # move_image(image)
    POSTS_DB.insert_one(post)
    post["_id"] = str(post.get("_id"))  # convert ObjectId to strings
    return post


def get_post(post_id):
    return POSTS_DB.find_one({"_id": ObjectId(post_id)})


def delete_post(post_id):
    post = POSTS_DB.find_one({"_id": ObjectId(post_id)})
    if post is not None:
        image_filename = post["image"]
        POSTS_DB.delete_one({"_id": ObjectId(post_id)})
        if image_filename is not None:
            IMAGES_DB.delete_one({"_id": image_filename})
        return True
    return False


def get_posts_from_users(usernames, n_posts, page):
    offset = n_posts * page
    usernames_l = list(map(lambda s: s.lower(), usernames))
    mongo_result = POSTS_DB.find({"username_l": {"$in": usernames_l}}).sort("date_created", -1).limit(n_posts + offset)
    posts = []
    for post in mongo_result[offset:offset + n_posts]:
        post["_id"] = str(post["_id"])
        posts.append(post)
    return posts


# IMAGES
def save_temporary_image(image_data):
    filename = utils.get_random_alphanumeric_string(16) + ".jpg"
    success = True
    try:
        Path("data/images/tmp").mkdir(parents=True, exist_ok=True)
        Image.open(image_data).convert("RGB").save("data/images/tmp/" + filename, "JPEG", quality=60)
    except Exception as e:
        success = False
        print(e, file=sys.stderr)
    return None if not success else filename


# Uploads image from tmp to mongoDB and deletes temporary image
def upload_image(filename):
    with open("data/images/tmp/" + filename, "rb") as f:
        encoded = Binary(f.read())
    IMAGES_DB.insert_one({"_id": filename, "data": encoded})
    os.remove("data/images/tmp/" + filename)


# Get image from mongoDB
def get_image(filename):
    res = IMAGES_DB.find_one({"_id": filename})
    return res["data"] if res is not None else None


# TOKEN
def verify_token(username, token):
    user = get_user(username)
    if user is None:
        return False
    t = int(time())
    if user["token"] == token and user["token_exp"] > t:
        if user["token_exp"] - t < 1800 or True:
            USERS_DB.update_one({"username_l": username.lower()}, {"$set": {"token_exp": t + 3600}})
        return True
    return False
