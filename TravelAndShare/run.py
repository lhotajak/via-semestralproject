from flask_app import app

"""Run Flask app"""
if __name__ == '__main__':
    app.run(port=80, debug=False, threaded=True, host="127.0.0.1")
