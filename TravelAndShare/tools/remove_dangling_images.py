import os

import pymongo


# Removes every image document, which isn't connected to any post in database
def remove_images():
    mongo_client = pymongo.MongoClient(os.environ["MONGODB_URI"])
    db = mongo_client["via_travel_and_share"]  # database
    POSTS_DB = db["posts"]
    IMAGES_DB = db["images"]
    images = []
    posts = POSTS_DB.find({"image": {"$ne": None}})
    for post in posts:
        images.append(post["image"])
    print("images size: ", len(images))
    ret = IMAGES_DB.delete_many({"_id": {"$nin": images}})
    print("deleted size: ", ret.deleted_count)


if __name__ == "__main__":
    remove_images()
