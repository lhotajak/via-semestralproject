import os
from os.path import isfile, join


def clear_images(path):
    files = [f for f in os.listdir(path) if isfile(join(path, f))]
    for f in files:
        os.remove(path + f)


# Removes every temporary image in /data/images/tmp/
if __name__ == "__main__":
    clear_images(path="../data/images/tmp/")
