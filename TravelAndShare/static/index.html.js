const BASE_ADDRESS = window.location.protocol + "//" + window.location.hostname + "/";
const DEFAULT_MAP_COORDS = {"x": 14.400307, "y": 50.071853};
const LOCAL_STORAGE_USER_KEY = "userData";

let MAP_COORDS = DEFAULT_MAP_COORDS;
let USER = null;
let FILE_SELECTED = false;

function el(id) {
    return document.getElementById(id);
}

function alertError(error_msg) {
    alert("Error: " + error_msg);
}

let LAST_PROMPT_FUNC = null;

function confirm(text, func) {
    const funcWrapper = e => {
        func();
        confirm_prompt.style.setProperty("display", "none");
    }
    const confirm_prompt = el("confirm_prompt");
    confirm_prompt.style.setProperty("display", "block");
    el("confirm_prompt_text").innerHTML = text;
    const btn = el("btn_prompt_confirm");
    if (LAST_PROMPT_FUNC !== null) {
        btn.removeEventListener("click", LAST_PROMPT_FUNC, true);
    }
    LAST_PROMPT_FUNC = funcWrapper;
    btn.addEventListener("click", LAST_PROMPT_FUNC, true);
}

// Calling the "main function" (called when everything is loaded)
window.addEventListener("load", function (e) {
    // Initialization
    addListeners();
    mapSwitch();

    USER = JSON.parse(localStorage.getItem(LOCAL_STORAGE_USER_KEY)); // loading user from local storage
    if (USER !== null) {
        refreshAndLoadUserData();
    }
});

/**
 * Adds every needed event listener
 */
function addListeners() {
    const wall = el("wall");
    const new_post = el("new_post");

    el("btn_create_new_post").addEventListener("click", function (e) {
        // Initialize input to default values
        el("title").value = "";
        el("text").value = "";
        el("map_switch").checked = false
        el("map_container").style.setProperty("display", "none");
        MAP_COORDS = DEFAULT_MAP_COORDS;
        el("image_form").reset(); // reset image uploader
        el("file_upload_label").innerHTML = "Vyber obrázek kliknutím sem";  // default text for file chooser
        FILE_SELECTED = false;

        // Hide wall, show new_post form
        wall.style.setProperty("display", "none");
        new_post.style.setProperty("display", "block");
    });

    el("btn_back_to_wall").addEventListener("click", function (e) {
        new_post.style.setProperty("display", "none");
        wall.style.setProperty("display", "block");
    });

    el("btn_prompt_storno").addEventListener("click", e => {
        el("confirm_prompt").style.setProperty("display", "none");
    });
    el("btn_delete_user").addEventListener("click", e => confirm("Opravdu chcete smazat Váš účet?", deleteUser))
    el("btn_register").addEventListener("click", e => register());
    el("btn_login").addEventListener("click", e => login());
    el("btn_logout").addEventListener("click", e => logout());
    el("btn_add_follow").addEventListener("click", e => addFollow());
    el("btn_new_post_submit").addEventListener("click", e => createPost());

    // Name of the file appears on select
    el("image").addEventListener("change", function (e) {
        el("file_upload_label").innerHTML = e.target.value.split("\\").pop();
        FILE_SELECTED = true;
    });
}

/**
 * Hides everything besides wall and logout panel
 */
function showWall() {
    // HIDE
    el("register_form").style.setProperty("display", "none");
    el("login_form").style.setProperty("display", "none");
    el("new_post").style.setProperty("display", "none");
    el("home_page").style.setProperty("display", "none");

    // SHOW
    el("logout_form").style.setProperty("display", "flex");
    el("wall").style.setProperty("display", "block");
    el("username_div").innerHTML = USER.username;
}

/* Asynchronous calls to communicate with server */
/**
 * Register user
 */
function register() {
    const username = el("username_reg").value;
    const password = el("password_reg").value;

    const address = BASE_ADDRESS + "users/register";
    const async = true;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", address, async);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function (event) {
        const respText = event.target.responseText;
        const resp = JSON.parse(respText);
        if (resp.code !== 0) {
            alertError(resp.error_msg);
        } else {
            USER = resp.data.user;
            localStorage.setItem(LOCAL_STORAGE_USER_KEY, JSON.stringify(USER));
            getAndDisplayUserData();
        }
    };
    const registerObject = {
        "username": username,
        "password": password
    };
    xhr.send(JSON.stringify(registerObject));
}

/**
 * Login user
 */
function login() {
    const username = el("username_login").value;
    const password = el("password_login").value;

    const address = BASE_ADDRESS + "users/login";
    const async = true;
    const xhr = new XMLHttpRequest();
    xhr.open("POST", address, async);
    xhr.setRequestHeader("Content-Type", "application/json");
    xhr.onload = function (event) {
        const respText = event.target.responseText;
        const resp = JSON.parse(respText);
        if (resp.code !== 0) {
            alertError(resp.error_msg);
        } else {
            USER = resp.data.user;
            localStorage.setItem(LOCAL_STORAGE_USER_KEY, JSON.stringify(USER));
            getAndDisplayUserData();
        }
    };
    const loginObject = {
        "username": username,
        "password": password
    };
    xhr.send(JSON.stringify(loginObject));
}


/**
 * Adds authorization header to XMLHttpRequest
 */
function addAuth(xhr) {
    xhr.setRequestHeader('Authorization', 'Basic ' + btoa(USER.username + ":" + USER.token));
}

/**
 * Updates user token and followlist
 */
function refreshAndLoadUserData() {
    const address = BASE_ADDRESS + "users/" + USER.username;
    const async = true;
    const xhr = new XMLHttpRequest();
    xhr.open("GET", address, async);
    xhr.setRequestHeader("Content-Type", "application/json");
    addAuth(xhr);
    xhr.onload = function (event) {
        const respText = event.target.responseText;
        const resp = JSON.parse(respText);
        if (resp.code === 101) {
            logout();
        } else if (resp.code !== 0) {
            alertError(resp.error_msg);
        } else {
            USER = resp.data.user;
            localStorage.setItem(LOCAL_STORAGE_USER_KEY, JSON.stringify(USER));
            getAndDisplayUserData();
        }
    };
    xhr.send();
}


/**
 * Gets user data then displays them
 */
function getAndDisplayUserData() {
    showWall();
    loadPostsData();
    loadFollowed();
}

/**
 * Deletes user
 */
function deleteUser() {
    const address = BASE_ADDRESS + "users/" + USER.username;
    const async = true;
    const xhr = new XMLHttpRequest();
    xhr.open("DELETE", address, async);
    xhr.setRequestHeader("Content-Type", "application/json");
    addAuth(xhr);
    xhr.onload = function (event) {
        const respText = event.target.responseText;
        const resp = JSON.parse(respText);
        if (resp.code !== 0) {
            alertError(resp.error_msg);
        } else {
            logout();
        }
    };
    xhr.send();
}

/**
 * Get and display posts data from server
 */
function loadPostsData() {
    function showPostsOnWall(posts) {
        el("posts_container").innerHTML = "";
        const mapsFunctions = [];
        for (let i = 0; i < posts.length; i++) {
            const p = posts[i];
            const f = showPost(p);
            if (f !== null) mapsFunctions.push(f);
        }
        mapsFunctions.forEach(f => f()); // call every map function
    }

    const xhr = new XMLHttpRequest();
    const usernames = JSON.parse(JSON.stringify(USER.follow));
    usernames.push(USER.username);
    const address = BASE_ADDRESS + "posts?n=10&page=0";
    const async = true;
    xhr.open("GET", address, async);
    xhr.setRequestHeader("Content-Type", "application/json");
    addAuth(xhr);
    xhr.onload = function (event) {
        const respText = event.target.responseText;
        const resp = JSON.parse(respText);
        if (resp.code === 101) {
            logout();
        } else if (resp.code !== 0) {
            alertError(resp.error_msg);
        } else {
            showPostsOnWall(resp.data.posts);
        }
    }
    xhr.send();
}

/**
 * Appends followed user to user's follow list
 */
function addFollow() {
    const address = BASE_ADDRESS + "users/" + USER.username + "/follow/" + el("username_add_text").value;
    const async = true;
    const xhr = new XMLHttpRequest();
    xhr.open("PUT", address, async);
    xhr.setRequestHeader("Content-Type", "application/json");
    addAuth(xhr);
    xhr.onload = function (event) {
        const respText = event.target.responseText;
        const resp = JSON.parse(respText);
        if (resp.code === 101) {
            logout();
        } else if (resp.code !== 0) {
            alertError(resp.error_msg);
        } else {
            el("username_add_text").value = "";
            refreshAndLoadUserData();
        }
    };
    xhr.send();
}

/**
 * Gets and displays information about followed users
 */
function loadFollowed() {
    const c = el("followed_container");
    el("follow_list").style.setProperty("display", "block");

    c.innerHTML = "";
    USER.follow.sort((a, b) => a.localeCompare(b));
    for (let i = 0; i < USER.follow.length; i++) {
        const username = USER.follow[i];
        const divUsername = document.createElement("div");
        divUsername.classList.add("font-weight-bold");
        divUsername.innerHTML = username;
        const a = document.createElement("a");
        a.classList.add("btn", "btn-sm", "btn-info", "position-absolute");
        a.style.setProperty("right", 0);
        a.innerHTML = "Odebrat";
        a.addEventListener("click", e => confirm(
            "Opravdu chcete přestat sledovat uživatele " + username + "?",
            () => unfollowUser(username))
        );
        const div = document.createElement("div");
        div.classList.add("form-inline", "w-75", "position-relative", "mb-3")
        div.appendChild(divUsername);
        div.appendChild(a);
        c.appendChild(div);
    }
    if (USER.follow.length === 0) {
        const div = document.createElement("div");
        div.classList.add("w-75", "mb-3")
        div.innerHTML = "Nemáte koho sledovat? Nevadí, zkuste uživatele s uživatelským jménem <span class=\"font-weight-bold\">demo</span>!"
        c.appendChild(div);
    }
}

/**
 * Removes followed user
 */
function unfollowUser(username) {
    const address = BASE_ADDRESS + "users/" + USER.username + "/follow/" + username;
    const async = true;
    const xhr = new XMLHttpRequest();
    xhr.open("DELETE", address, async);
    xhr.setRequestHeader("Content-Type", "application/json");
    addAuth(xhr);
    xhr.onload = function (event) {
        const respText = event.target.responseText;
        const resp = JSON.parse(respText);
        if (resp.code === 101) {
            logout();
        } else if (resp.code !== 0) {
            alertError(resp.error_msg);
        } else {
            refreshAndLoadUserData();
        }
    };
    xhr.send();
}

/**
 * Uploads image and sends newly created post to server
 */
function createPost() {
    // Send post
    function sendRequest(filename) {
        const xhr = new XMLHttpRequest();
        const address = BASE_ADDRESS + "posts";
        const async = true;
        xhr.open("POST", address, async);
        xhr.setRequestHeader("Content-Type", "application/json");
        addAuth(xhr);
        xhr.onload = function (event) {
            const respText = event.target.responseText;
            const resp = JSON.parse(respText);
            if (resp.code === 101) {
                logout();
            } else if (resp.code !== 0) {
                alertError(resp.error_msg);
            } else {
                getAndDisplayUserData();
            }
        };
        const postObj = {
            "title": el("title").value,
            "text": el("text").value
        };
        if (el("map_switch").checked) {
            postObj.map_coords = MAP_COORDS;
        }
        if (filename !== null) {
            postObj.image = filename;
        }
        xhr.send(JSON.stringify(postObj));
    }

    if (FILE_SELECTED) { // When file is selected -> send file first
        const xhr = new XMLHttpRequest();
        const address = BASE_ADDRESS + "images";
        const async = true;
        xhr.open("POST", address, async);
        addAuth(xhr);
        xhr.onload = function (event) {
            const respText = event.target.responseText;
            const resp = JSON.parse(respText);
            if (resp.code !== 0) {
                alertError(resp.error_msg);
            } else {
                sendRequest(resp.data.filename);
            }
        };

        const data = new FormData();
        data.append("image", el("image").files[0]);
        xhr.send(data);
    } else { // Without file
        sendRequest(null)
    }
}


/**
 * Deletes post
 */
function deletePost(post_id) {
    const xhr = new XMLHttpRequest();
    const address = BASE_ADDRESS + "posts/" + post_id;
    const async = true;
    xhr.open("DELETE", address, async);
    xhr.setRequestHeader("Content-Type", "application/json");
    addAuth(xhr);
    xhr.onload = function (event) {
        const respText = event.target.responseText;
        const resp = JSON.parse(respText);
        if (resp.code === 101) {
            logout();
        } else if (resp.code !== 0) {
            alertError(resp.error_msg);
        } else {
            getAndDisplayUserData();
        }
    };
    xhr.send();
}

/**
 * Displays post in main view
 */
function showPost(post) {
    // random string of given length
    function randStr(length) {
        let result = "";
        const characters = "abcdefghijklmnopqrstuvwxyz";
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
            result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    // helper function for creating elements
    function c(elemName, classList, parrent) {
        const elem = document.createElement(elemName);
        elem.classList = classList;
        if (parrent !== null) parrent.appendChild(elem);
        return elem;
    }

    // unixTime to date string
    function formatDate(unixTimeSec) {
        const date = new Date(unixTimeSec * 1000);
        return date.toLocaleString("cs-CZ");
    }

    function addTemperatureInfo(divElem, coords) {
        const xhr = new XMLHttpRequest();
        // 5 days weather forcast
        const address = "https://api.openweathermap.org/data/2.5/forecast?units=metric&lat=" + coords.y + "&lon=" + coords.x + "&appid=45b3eafc638fb9495d51b59739fd8faf";
        const async = true;
        xhr.open("GET", address, async);
        xhr.onload = function (event) {
            const respText = event.target.responseText;
            const resp = JSON.parse(respText);
            const resultList = resp.list;
            const n = 9; // 2 days forecast
            divElem.innerHTML = "<div class=\"font-weight-bold\">Teplota a počasí:</div>";
            for (let i = 0; i < n; i++) {
                const r = resultList[i];
                const splitted = formatDate(r.dt).split(" ");
                const dateString = splitted[0] + " " + splitted[1];
                let timeString = splitted[3];
                if (timeString.length == 8) {
                    timeString = timeString.substring(0, 5);
                } else {
                    timeString = "&nbsp;" + timeString.substring(0, 4); // add space
                }
                divElem.innerHTML += ""
                    + "<div>"
                    + "<span class=\"text-monospace pl-2\">" + dateString + " " + timeString + " ➤ <span class=\"font-weight-bold\">" + Number(r.main.temp).toFixed(2) + "°C</span></span>"
                    + "<img src='https://openweathermap.org/img/wn/" + r.weather[0].icon + ".png' height='35px' alt='weather'>"
                    + "</div>";
            }
        };
        xhr.send();
    }

    // Creating elements
    const divPost = c("div", "shadow-lg px-5 py-3 mb-4 rounded", null);
    c("div", "h2", divPost).innerHTML = post.title;
    c("div", "text-muted h5 my-0", divPost).innerHTML = post.username;
    const divDate = c("div", "text-muted mb-3", divPost);
    divDate.innerHTML = formatDate(post.date_created);
    if (post.image !== null) {
        const divRow = c("div", "row", divPost);
        const divWrapperImg = c("div", "col-6", divRow);
        const img = c("img", "mw-100 shadow-lg rounded", divWrapperImg);
        img.src = BASE_ADDRESS + "images/" + post.image;
        const divText = c("div", "col-6", divRow);
        divText.innerHTML = "<p>" + post.text + "</p>";
    } else {
        const divText = c("div", "my-1", divPost);
        divText.innerHTML = "<p>" + post.text + "</p>";
    }
    const randomMapId = randStr(32); // random 16 chars long string
    if (post.map_coords !== null) {
        const divMapWrapper = c("div", "row mt-3 p-3 map_wrapper", divPost);
        const divTempInfo = c("div", "col-5 mt-2", divMapWrapper);
        addTemperatureInfo(divTempInfo, post.map_coords);
        const divMap = c("div", "col-7 mt-4 mapy_map", divMapWrapper);
        divMap.id = randomMapId;
    }
    if (post.username === USER.username) { // USER's post
        const divWrapperBtn = c("div", "row mt-2", divPost)
        c("div", "col-9", divWrapperBtn); // Padding
        const delButton = c("input", "btn btn-sm btn-info float-right col-2", divWrapperBtn);
        delButton.type = "button";
        delButton.value = "Odstranit";
        delButton.addEventListener("click", e => confirm(
            "Opravdu chcete smazat tento příspěvek?",
            () => deletePost(post._id))
        );
    }
    ;

    el("posts_container").appendChild(divPost);
    if (post.map_coords !== null) {
        // There was a problem with loading the map dynamically
        // So I'm just creating functions, which will be called when all the content is added to page
        const insertMap = function () {
            const center = SMap.Coords.fromWGS84(post.map_coords.x, post.map_coords.y);
            const m = new SMap(JAK.gel(randomMapId), center, 12);
            m.addDefaultLayer(SMap.DEF_BASE).enable();
            const l = new SMap.Layer.Marker();
            m.addLayer(l).enable();
            const mark = new SMap.Marker(center);
            l.addMarker(mark);
        }
        return insertMap
    }
    return null;
}

/**
 * Initializes map in post creator
 */
function mapyInit() {
    el("m").innerHTML = "";
    const center = SMap.Coords.fromWGS84(DEFAULT_MAP_COORDS.x, DEFAULT_MAP_COORDS.y);
    const m = new SMap(JAK.gel("m"), center);
    m.addDefaultLayer(SMap.DEF_BASE).enable();
    const mouse = new SMap.Control.Mouse(SMap.MOUSE_PAN | SMap.MOUSE_WHEEL | SMap.MOUSE_ZOOM); /* Mouse control */
    m.addControl(mouse);

    const l = new SMap.Layer.Marker();
    m.addLayer(l).enable();

    const mark = new SMap.Marker(center);
    mark.decorate(SMap.Marker.Feature.Draggable);
    l.addMarker(mark);

    function start(e) { /* Drag start */
        const node = e.target.getContainer();
        node[SMap.LAYER_MARKER].style.cursor = "help";
    }

    function stop(e) {
        const node = e.target.getContainer();
        node[SMap.LAYER_MARKER].style.cursor = "";
        MAP_COORDS = e.target.getCoords();
    }

    const signals = m.getSignals();
    signals.addListener(window, "marker-drag-stop", stop);
    signals.addListener(window, "marker-drag-start", start);
}

// Map switch (show/hide map)
function mapSwitch() {
    const map_switch = el("map_switch");
    const map_container = el("map_container");
    map_switch.addEventListener("input", function (e) {
        map_container.style.setProperty("display", map_switch.checked ? "block" : "none");
        mapyInit(); // Initialize Mapy when user clicks on the switch
    });
    map_container.style.setProperty("display", map_switch.checked ? "block" : "none");
}

// User logout
function logout() {
    localStorage.removeItem(LOCAL_STORAGE_USER_KEY); // Delete from local storage so user will no log in automatically
    location.reload(); // refreshing browser resets everything
}
