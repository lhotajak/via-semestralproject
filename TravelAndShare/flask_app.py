from flask import Flask, request, send_file, render_template, Response
from flask_cors import CORS, cross_origin
from flask_restplus import Api, Resource

import services
import utils

"""Define Flask app"""
app = Flask(__name__)
api = Api(app=app,
          version="1.0",
          title="Travel & Share",
          description="Share your travel experiences!")

"""Enable CORS"""
CORS(app)

"""Define namespace"""
posts_name_space = api.namespace("posts", description="Requests for posts")
images_name_space = api.namespace("images", description="Requests for images")
users_name_space = api.namespace("users", description="Requests for users")

# Frequently used responses
WRONG_API_CALL = {"error_msg": "Wrong API call", "code": 100}
INVALID_TOKEN_RESPONSE = {"error_msg": "Authorization failed - invalid username or token", "code": 101}


# UI
@app.route("/app/")
def index():
    return render_template("index.html")


# USERS
@users_name_space.route("/register")
class RegisterUser(Resource):
    @api.doc(responses={200: "OK"}, description="Create new user, return user data")
    @cross_origin()
    def post(self):
        d = request.json
        if d is None or "username" not in d or "password" not in d:
            return WRONG_API_CALL
        username = d["username"]
        if not utils.is_valid_username(username):
            return {"error_msg": "Username in wrong format", "code": 2}
        password = d["password"]
        if not utils.is_valid_password(password):
            return {"error_msg": "Password in wrong format", "code": 3}
        if services.user_in_database(username):
            return {"error_msg": "User with given username is already in database", "code": 4}
        user = services.create_user(username, password)
        if user is None:
            return {"error_msg": "User could not be created", "code": 5}
        else:
            return {
                "code": 0,
                "data": {
                    "user": {
                        "username": user["username"],
                        "token": user["token"],
                        "follow": user["follow"]
                    }
                }
            }


@users_name_space.route("/login")
class LoginUser(Resource):
    @api.doc(responses={200: "OK"}, description="Verify user, return user data")
    @cross_origin()
    def post(self):
        d = request.json
        if d is None or "username" not in d or "password" not in d:
            return {"error_msg": "Wrong API call", "code": 100}
        user = services.verify_user(d["username"], d["password"])
        if user is None:
            return {"error_msg": "Combination of username and password is not in database", "code": 2}
        return {
            "code": 0,
            "data": {
                "user": {
                    "username": user["username"],
                    "token": user["token"],
                    "follow": user["follow"]
                }
            }
        }


@users_name_space.route("/<string:username>")
class UserInfo(Resource):
    @api.doc(responses={200: "OK"}, description="Get user data")
    @cross_origin()
    def get(self, username):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        if username.lower() != user["username_l"]:
            return {"code": 102, "error_msg": "Unauthorized attempt to get userinfo"}
        return {
            "code": 0,
            "data": {
                "user": {
                    "username": user["username"],
                    "token": user["token"],
                    "follow": user["follow"]
                }
            }
        }

    @api.doc(responses={200: "OK"}, description="Deletes user from database")
    @cross_origin()
    def delete(self, username):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        if username.lower() != user["username_l"]:
            return {"code": 102, "error_msg": "Unauthorized attempt to delete user"}
        services.delete_user(username)
        return {"code": 0}


@users_name_space.route("/<string:username>/follow/<string:followed>")
class FollowUsername(Resource):
    @api.doc(responses={200: "OK"}, description="Add user with username {followed} into follow list of {username}")
    @cross_origin()
    def put(self, username, followed):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        if username.lower() != user["username_l"]:
            return {"code": 102, "error_msg": "Unauthorized attempt to add follow relationship"}
        ret = services.follow_user(username, followed)
        if ret == 0:
            return {"code": 0}
        elif ret == 1:
            return {"code": 1, "error_msg": "You can not follow yourself"}
        elif ret == 2:
            return {"code": 2, "error_msg": "User with given username was not found"}
        elif ret == 3:
            return {"code": 3, "error_msg": "You are already following user with given username"}
        else:
            return {"code": ret, "error_msg": "Unknown error"}

    @api.doc(responses={200: "OK"},
             description="Remove user with username {followed} from follow list of {username}")
    @cross_origin()
    def delete(self, username, followed):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        if username.lower() != user["username_l"]:
            return {"code": 102, "error_msg": "Unauthorized attempt to add follow relationship"}
        ret = services.unfollow_user(username, followed)
        if ret == 0:
            return {"code": 0}
        elif ret == 1:
            return {"code": 1, "error_msg": "You can not unfollow yourself"}
        elif ret == 2:
            return {"code": 2, "error_msg": "User with given username was not found"}
        elif ret == 3:
            return {"code": 3, "error_msg": "You are not following user with given username"}
        else:
            return {"code": ret, "error_msg": "Unknown error"}


@posts_name_space.route("")
class PostsList(Resource):
    @api.doc(responses={200: "OK"},
             description="Get list of most recent posts with pages offset",
             params={"n": "Number of posts (default is 15)", "page": "Page offset (default is 0)"})
    @cross_origin()
    def get(self):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        n = request.args.get('n', default=15, type=int)
        page = request.args.get('page', default=0, type=int)
        posts = services.get_posts_from_users(user["follow"] + [user["username"]], n, page)
        if posts is None:
            return {"code": 2, "error_msg": "Something went wrong getting posts from users"}
        else:
            return {"code": 0, "data": {"posts": posts}}

    @api.doc(responses={200: "OK"}, description="Create new post")
    @cross_origin()
    def post(self):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        d = request.json
        if d is None or "title" not in d or "text" not in d:
            return WRONG_API_CALL
        if len(d["title"]) == 0 or len(d["text"]) == 0:
            return {"error_msg": "Title and Text must not be empty", "code": 2}
        image = None
        if "image" in d:
            image = d["image"]
        map_coords = None
        if "map_coords" in d:
            map_coords = d["map_coords"]
        post = services.create_post(user["username"], d["title"], d["text"], map_coords, image)
        if post is None:
            return {"code": 3, "error_msg": "Something went wrong while uploading your post"}
        else:
            return {"code": 0, "data": {"post": post}}


@posts_name_space.route("/<string:post_id>")
class DeletePost(Resource):
    @api.doc(responses={200: "OK"}, description="Get post with id {post_id}")
    @cross_origin()
    def get(self, post_id):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        post = services.get_post(post_id)
        if post is None:
            return {"code": 1, "error_msg": "Post with this id does not exist in database"}
        if post["username_l"] != user["username_l"]:
            return {"code": 2, }
        ret = services.delete_post(post_id)
        if not ret:
            return {"code": 3, "error_msg": "There was a problem deleting this post"}
        else:
            return {"code": 0, "data": {"post": post}}

    @api.doc(responses={200: "OK"}, description="Delete post with id {post_id} from database")
    @cross_origin()
    def delete(self, post_id):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        post = services.get_post(post_id)
        if post is None:
            return {"code": 1, "error_msg": "Post with this id does not exist in database"}
        if post["username_l"] != user["username_l"]:
            return {"code": 2, }
        ret = services.delete_post(post_id)
        if not ret:
            return {"code": 1, "error_msg": "Post with this id does not exist in database"}
        else:
            return {"code": 0}


# IMAGES
@images_name_space.route("/<string:img_filename>")
class GetImage(Resource):
    # Get image with given filename
    @api.doc(responses={200: "OK"}, description="Get image (binary file)")
    @cross_origin()
    def get(self, img_filename):
        img = services.get_image(img_filename)
        if img is None:
            return send_file("data/images/not_found.jpg", mimetype="image/jpeg")  # Default not found image
        r = Response(response=img, status=200, mimetype="image/jpeg")
        r.headers["Content-Type"] = "image/jpeg"
        return r


# This is not content type JSON but content type multipart data
@images_name_space.route("")
class UploadImage(Resource):
    # Upload new image
    @api.doc(responses={200: "OK"}, description="Upload image to server, get filename of uploaded file")
    @cross_origin()
    def post(self):
        user = services.authorize(request.headers)
        if user is None:
            return INVALID_TOKEN_RESPONSE
        if "image" in request.files:
            image_data = request.files["image"]
            img_filename = services.save_temporary_image(image_data)
            if img_filename is not None:
                return {"code": 0, "data": {"filename": img_filename}}
        return {"code": 1, "error_msg": "There was a problem with uploading your image file"}
